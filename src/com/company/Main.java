package com.company;

public class Main {

    public static void main(String[] args) {

        double costOfFuelPerLiter;
        double costOfFuel;
        double allDistance;

        System.out.println("-------");
        System.out.print("Enter the cost of fuel, UAH: " + args[0]);
        System.out.println();
        System.out.println("-------");

        costOfFuelPerLiter = Double.parseDouble(args[0]);

        Car car = new Car();
        Car.tankVolume = 50.5;
        Car.restInTank = 15.5;
        Car.fuelConsumptionPerHundred = 7.7;
        Car.pointOne = "Odesa";
        System.out.println("You are leaving " + Car.pointOne);
        System.out.println("Tank volume: " + Car.tankVolume);
        System.out.println("Fuel consumption per hundred km, liters: " + Car.fuelConsumptionPerHundred);
        System.out.println("----------");
        Car.fillTheTank();
        System.out.println();
        Car.distanceOdesaKryveOzero = 178;
        Car.kmToRide = Car.distanceOdesaKryveOzero;
        Car.pointTwo = "KryveOzero";
        System.out.println("You are going to " + Car.pointTwo);
        System.out.println();
        System.out.println("----------");

        System.out.println("You came to " + Car.pointTwo);
        System.out.println();
        Car.fuelBalance();
        Car.refueling();
        Car.fillTheTank();
        System.out.println("----------");
        System.out.println();
        Car.distanceKryveOzeroZaschkiv = 157;
        Car.kmToRide = Car.distanceKryveOzeroZaschkiv;
        Car.pointThree = "Zaschkiv";
        System.out.println("You are going to " + Car.pointThree);
        System.out.println();
        Car.refueling();
        System.out.println("----------");

        System.out.println("You came to " + Car.pointThree);
        System.out.println();
        Car.fuelBalance();

        Car.fillTheTank();
        System.out.println("----------");
        System.out.println();
        Car.distanceZaschkivKyiv = 150;
        Car.kmToRide = Car.distanceZaschkivKyiv;
        Car.pointFour = "Kyiv";
        System.out.println("You are going to " + Car.pointFour);
        System.out.println();
        Car.refueling();
        System.out.println("----------");
        System.out.println("You came to " + Car.pointFour);
        System.out.println();
        Car.fuelBalance();
        System.out.println("-------");


        allDistance = Car.fuelConsumptionPerHundred / 100 * (Car.distanceOdesaKryveOzero + Car.distanceKryveOzeroZaschkiv + Car.distanceZaschkivKyiv);
        costOfFuel = allDistance * costOfFuelPerLiter;
        System.out.println("Поїздка коштує " + costOfFuel + " гривень.");
    }
}
