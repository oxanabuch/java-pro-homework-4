package com.company;

public class Car {

    static double tankVolume;
    static double restInTank;
    static double fuelConsumptionPerHundred;

    static double kmToRide;
    static double fuelToDestination;
    static double restInTankN;
    static double fuelToRefueling;
    static double fullTank;
    static double spentFuel;

    static String pointOne;
    static String pointTwo;
    static String pointThree;
    static String pointFour;

    static double distanceOdesaKyiv;
    static double distanceOdesaKryveOzero;
    static double distanceKryveOzeroZaschkiv;
    static double distanceZaschkivKyiv;

    public static double getTankVolume() {
        return tankVolume;
    }

    public static void setTankVolume(double tankVolume) {
        Car.tankVolume = tankVolume;
    }

    public static double getRestInTank() {
        return restInTank;
    }

    public static void setRestInTank(double restInTank) {
        Car.restInTank = restInTank;
    }

    public static double getFuelConsumptionPerHundred() {
        return fuelConsumptionPerHundred;
    }

    public static void setFuelConsumptionPerHundred(double fuelConsumptionPerHundred) {
        Car.fuelConsumptionPerHundred = fuelConsumptionPerHundred;
    }

    public static double getKmToRide() {
        return kmToRide;
    }

    public static void setKmToRide(double kmToRide) {
        Car.kmToRide = kmToRide;
    }

    public static String getPointOne() {
        return pointOne;
    }

    public static void setPointOne(String pointOne) {
        Car.pointOne = pointOne;
    }

    public static String getPointTwo() {
        return pointTwo;
    }

    public static void setPointTwo(String pointTwo) {
        Car.pointTwo = pointTwo;
    }

    public static String getPointThree() {
        return pointThree;
    }

    public static void setPointThree(String pointThree) {
        Car.pointThree = pointThree;
    }

    public static String getPointFour() {
        return pointFour;
    }

    public static void setPointFour(String pointFour) {
        Car.pointFour = pointFour;
    }

    public static double getDistanceOdesaKyiv() {
        return distanceOdesaKyiv;
    }

    public static void setDistanceOdesaKyiv(double distanceOdesaKyiv) {
        Car.distanceOdesaKyiv = distanceOdesaKyiv;
    }

    public static double getDistanceOdesaKryveOzero() {
        return distanceOdesaKryveOzero;
    }

    public static void setDistanceOdesaKryveOzero(double distanceOdesaKryveOzero) {
        Car.distanceOdesaKryveOzero = distanceOdesaKryveOzero;
    }

    public static double getDistanceKryveOzeroZaschkiv() {
        return distanceKryveOzeroZaschkiv;
    }

    public static void setDistanceKryveOzeroZaschkiv(double distanceKryveOzeroZaschkiv) {
        Car.distanceKryveOzeroZaschkiv = distanceKryveOzeroZaschkiv;
    }

    public static double getDistanceZaschkivKyiv() {
        return distanceZaschkivKyiv;
    }

    public static void setDistanceZaschkivKyiv(double distanceZaschkivKyiv) {
        Car.distanceZaschkivKyiv = distanceZaschkivKyiv;
    }

    public static void fillTheTank() {

        double fullTank = tankVolume - restInTank;

        System.out.println("Pour " + fullTank + " liters of fuel to a full tank");
        System.out.println("You filled a full tank. Thank you!");
        System.out.println();
    }

    public static void fuelBalance() {
        spentFuel = fuelConsumptionPerHundred / 100 * kmToRide;
        restInTank = tankVolume - spentFuel;

        System.out.println("Fuel balance after overcoming " + kmToRide + " km, liters: " + restInTank);
        System.out.println();
    }

    public static void refueling() {

        fuelToDestination = fuelConsumptionPerHundred / 100 * kmToRide;
        fuelToRefueling = restInTankN - fuelToDestination;
        fullTank = tankVolume - restInTankN;

        System.out.println("You need " + fuelToDestination + " liters to get to your destination.");
        System.out.println();
    }
}
